<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rating extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT rating.rating, rating.at, tiket.* FROM rating
			JOIN tiket ON tiket.id = rating.id_tiket
			ORDER BY rating.at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(rating.id) as total FROM rating 
			JOIN tiket ON tiket.id = rating.id_tiket
			ORDER BY rating.at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data rating", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction
		));
	}

	public function detail($id_tiket) 
	{
		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM rating
			WHERE id_tiket = $id_tiket
		")->row();
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data rating", null);
	}

	public function create($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["rating"]) || empty($body["rating"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Perpare to send db
		$body["id_tiket"] = $id;

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[2] // Pegawai
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat rating", null);
	}
}
