<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "username LIKE '%$search%' OR email LIKE '%$search%' OR full_name LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM users WHERE $where
			ORDER BY at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM users WHERE $where
			ORDER BY at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data pengguna", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function create()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["level"]) || empty($body["level"]) ||
		!isset($body["full_name"]) || empty($body["full_name"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Prepare to input db
		$usernameAndPassword = $this->Request->generateRandomString(5);
		$body["username"] = $usernameAndPassword;
		$body["password"] = md5($usernameAndPassword);
		$body["platform"] = $this->DataEnum->platform()[0]; // Website
		$body["api_key"] = md5(date("YmdHis") . microtime(true) . $usernameAndPassword);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[5] // Users
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat pengguna", null);
	}

	public function registration()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["email"]) || empty($body["email"]) ||
		!isset($body["uuid"]) || empty($body["uuid"]) ||
		!isset($body["full_name"]) || empty($body["full_name"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Prepare to input db
		$body["platform"] = $this->DataEnum->platform()[1]; // Mobile
		$body["level"] = $this->DataEnum->level()[4]; // Client
		$body["api_key"] = md5(date("YmdHis") . microtime(true) . $body["email"]);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[5] // Users
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil melakukan registrasi", null);
	}

	public function login_pegawai()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["username"]) || empty($body["username"]) ||
		!isset($body["password"]) || empty($body["password"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Prepare get db
		$username = $body["username"];
		$password = md5($body["password"]);

		// Sent to db
		$data = $this->CustomSQL->query("
			SELECT users.* FROM users
			LEFT JOIN pegawai ON pegawai.id_users = users.id
			WHERE users.username = '$username' AND users.password = '$password'
		")->row();

		// Return If Error
		if (!isset($data) || empty($data)) {
			$data = $this->CustomSQL->query("
				SELECT users.* FROM users
				JOIN pegawai ON pegawai.id_users = users.id
				WHERE pegawai.nip = '$username' AND users.password = '$password'
			")->row();

			// Return If Error
			if (!isset($data) || empty($data)) $this->Request->res(500, null, "Akun tidak ditemukan", null);
		}
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat akun", null);
	}

	public function login()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["email"]) || empty($body["email"]) ||
		!isset($body["uuid"]) || empty($body["uuid"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Prepare get db
		$email = $body["email"];
		$uuid = $body["uuid"];

		// Sent to db
		$data = $this->CustomSQL->query("
			SELECT * FROM users WHERE email = '$email' AND uuid = '$uuid'
		")->row();

		// Return If Error
		if (!isset($data) || empty($data)) $this->Request->res(500, null, "Akun tidak ditemukan", null);
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat akun", null);
	}

	public function profile() 
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";

		// Check
		if (!isset($api_key) || empty($api_key))
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get From DB
		$data = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($data) || empty($data))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data akun", null);
	}

	public function update($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body) || empty($body)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		if (isset($body["password"]) || !empty($body["password"]))
			$body["password"] = md5($body["password"]);

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[5] // Users
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Get From DB
		$data = $this->CustomSQL->query("
			SELECT * FROM users WHERE id = '$id'
		")->row();

		// Return If Success
		$this->Request->res(200, $data, "Berhasil mengubah pengguna", null);
	}

	public function delete($id)
	{
		// Get Pegawai
		$pegawai = $this->CustomSQL->query("
			SELECT * FROM pegawai WHERE id_users = '$id'
		")->result_array();

		if (count($pegawai) > 0) {
			$pegawai = $pegawai[0];

			// Sent to db
			$checkID = $this->CustomSQL->delete(
				array("id_pegawai" => $pegawai["id"]),
				$this->DataEnum->tableName()[6] // Seksi Pegawai
			);

			// Sent to db
			$checkID = $this->CustomSQL->update(
				array(
					"id_solver" => $pegawai["id"],
					"status !=" => "antrian",
					"status !=" => "selesai"
				),
				array(
					"id_solver" => NULL,
					"is_approve" => 0,
					"status" => "antrian",
					"updated_at" => date("Y-m-d H:i:s")
				),
				$this->DataEnum->tableName()[4] // Tiket
			);

			// Sent to db
			$checkID = $this->CustomSQL->delete(
				array("id" => $pegawai["id"]),
				$this->DataEnum->tableName()[1] // Pegawai
			);

		}

		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id" => $id),
			$this->DataEnum->tableName()[5] // Users
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, null, "Berhasil menghapus pengguna", null);
	}
}
