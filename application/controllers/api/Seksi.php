<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seksi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "label LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM seksi WHERE $where
			ORDER BY at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM seksi WHERE $where
			ORDER BY at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data seksi", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function pegawai($id) 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "pegawai.nip LIKE '%$search%' OR pegawai.nama LIKE '%$search%' OR pegawai.alamat LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT pegawai.*, jabatan.label, users.username, users.full_name, seksi_pegawai.id as id_parent FROM seksi_pegawai
			JOIN pegawai ON pegawai.id = seksi_pegawai.id_pegawai 
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND seksi_pegawai.id_seksi = $id
			ORDER BY pegawai.at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(seksi_pegawai.id) as total FROM seksi_pegawai 
			JOIN pegawai ON pegawai.id = seksi_pegawai.id_pegawai 
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND seksi_pegawai.id_seksi = $id
			ORDER BY pegawai.at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data seksi pegawai", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function pegawai_create($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["id_pegawai"]) || empty($body["id_pegawai"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Prepare sent
		$body["id_seksi"] = $id;

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[6] // seksi
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat seksi pegawai", null);
	}

	public function pegawai_delete($id)
	{
		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id" => $id),
			$this->DataEnum->tableName()[6] // seksi
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, null, "Berhasil menghapus seksi pegawai", null);
	}

	public function create()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["label"]) || empty($body["label"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[3] // seksi
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat seksi", null);
	}

	public function update($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body) || empty($body)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[3] // seksi
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil mengubah seksi", null);
	}

	public function delete($id)
	{
		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id_seksi" => $id),
			$this->DataEnum->tableName()[6] // Seksi Pegawai
		);

		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id" => $id),
			$this->DataEnum->tableName()[3] // Seksi
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, null, "Berhasil menghapus Seksi", null);
	}
}
