<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiket extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "no LIKE '%$search%' OR pemohon LIKE '%$search%' OR email LIKE '%$search%' OR phone LIKE '%$search%' OR alamat LIKE '%$search%' OR kepentingan LIKE '%$search%' OR ringkasan LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM tiket WHERE $where
			ORDER BY created_at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM tiket WHERE $where
			ORDER BY created_at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data tiket", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function my() 
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";

		// Check
		if (!isset($api_key) || empty($api_key))
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get Users From DB
		$user = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($user) || empty($user))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM tiket WHERE id_users = ".$user->id."
			ORDER BY updated_at DESC
		")->result_array();
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data tiket", null);
	}

	public function waiting()
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "no LIKE '%$search%' OR pemohon LIKE '%$search%' OR email LIKE '%$search%' OR phone LIKE '%$search%' OR alamat LIKE '%$search%' OR kepentingan LIKE '%$search%' OR ringkasan LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM tiket WHERE ($where) AND status = 'antrian'
			ORDER BY created_at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM tiket WHERE ($where) AND status = 'antrian'
			ORDER BY created_at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data tiket", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function task() 
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";

		// Check
		if (!isset($api_key) || empty($api_key))
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get Users From DB
		$user = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($user) || empty($user))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM tiket WHERE id_solver = ".$user->id."
			AND status != 'selesai'
			ORDER BY updated_at DESC
		")->result_array();
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data tiket", null);
	}

	public function create()
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["pemohon"]) || empty($body["pemohon"]) ||
		!isset($body["email"]) || empty($body["email"]) ||
		!isset($body["phone"]) || empty($body["phone"]) ||
		!isset($body["alamat"]) || empty($body["alamat"]) ||
		!isset($body["kepentingan"]) || empty($body["kepentingan"]) ||
		!isset($body["ringkasan"]) || empty($body["ringkasan"]) ||
		!isset($api_key) || empty($api_key)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get Users From DB
		$user = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($user) || empty($user))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		// Prepare to Sent DB
		$body["status"] = $this->DataEnum->statusTiket()[0];
		$body["id_users"] = $user->id;
		$body["no"] = $this->Request->generateRandomString(10);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[4] // tiket
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat tiket", null);
	}

	public function approve($id)
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";

		// Check
		if (!isset($api_key) || empty($api_key)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get Users From DB
		$user = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($user) || empty($user))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		$body = array();
		$body["id_solver"] = $user->id;
		$body["is_approve"] = 1;
		$body["status"] = $this->DataEnum->statusTiket()[1];
		$body["updated_at"] = date("Y-m-d H:i:s");

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[4] // tiket
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil mengubah tiket", null);
	}

	public function update($id)
	{
		// Get Params
		$api_key = $this->input->get("api_key") ?: "";
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body) || empty($body) ||
		!isset($api_key) || empty($api_key)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);
		
		// Get Users From DB
		$user = $this->CustomSQL->query("
			SELECT * FROM users WHERE api_key = '$api_key'
		")->row();

		// Check Is Valid
		if (!isset($user) || empty($user))
			$this->Request->res(500, null, "Akun tidak ditemukan", null);

		if (isset($body["balasan"]) || !empty($body["balasan"])) {
			if (!isset($body["balasan"]) || empty($body["balasan"])) {
				$body["id_solver"] = $user->id;
			}
		}

		$body["updated_at"] = date("Y-m-d H:i:s");

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[4] // tiket
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil mengubah tiket", null);
	}
}
