<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function analytics()
	{
		$totalPegawai = $this->CustomSQL->query("
			SELECT COUNT(pegawai.id) as total FROM pegawai
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan 
			JOIN users ON users.id = pegawai.id_users
		")->row()->total;
		
		$totalClient = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM users
			WHERE level = 'client'
		")->row()->total;

		$totalTiket = array();
		$totalTiket["antrian"] = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM tiket
			WHERE status = 'antrian'
		")->row()->total;
		$totalTiket["proses"] = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM tiket
			WHERE status != 'antrian' AND status != 'selesai'
		")->row()->total;
		$totalTiket["selesai"] = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM tiket
			WHERE status = 'selesai'
		")->row()->total;
		
		$rating = array();
		$rating["total"] = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM rating
		")->row()->total;
		$rating["avg"] = $this->CustomSQL->query("
			SELECT AVG(rating) as total FROM rating
		")->row()->total;

		// Return If Success
		$this->Request->res(200, array(
			"pegawai" => $totalPegawai,
			"client" => $totalClient,
			"tiket" => $totalTiket,
			"rating" => $rating
		), "Berhasil memuat data analytics", null);
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "pegawai.nip LIKE '%$search%' OR pegawai.nama LIKE '%$search%' OR pegawai.alamat LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT pegawai.*, jabatan.label, users.username, users.full_name FROM pegawai 
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan
			JOIN users ON users.id = pegawai.id_users
			WHERE $where
			ORDER BY pegawai.at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(pegawai.id) as total FROM pegawai
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan 
			JOIN users ON users.id = pegawai.id_users
			WHERE $where
			ORDER BY pegawai.at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data pegawai", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function supervisor() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "pegawai.nip LIKE '%$search%' OR pegawai.nama LIKE '%$search%' OR pegawai.alamat LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT pegawai.*, jabatan.label, users.username, users.full_name FROM pegawai 
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND users.level = 'supervisor'
			ORDER BY pegawai.at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(pegawai.id) as total FROM pegawai
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan 
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND users.level = 'supervisor'
			ORDER BY pegawai.at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data pegawai", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function seksi() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "pegawai.nip LIKE '%$search%' OR pegawai.nama LIKE '%$search%' OR pegawai.alamat LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT pegawai.*, jabatan.label, users.username, users.full_name FROM pegawai 
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND users.level = 'seksi'
			ORDER BY pegawai.at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(pegawai.id) as total FROM pegawai
			JOIN jabatan ON jabatan.id = pegawai.id_jabatan 
			JOIN users ON users.id = pegawai.id_users
			WHERE ($where) AND users.level = 'seksi'
			ORDER BY pegawai.at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data pegawai", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function create()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["nip"]) || empty($body["nip"]) ||
		!isset($body["nama"]) || empty($body["nama"]) ||
		!isset($body["jk"]) || empty($body["jk"]) ||
		!isset($body["id_users"]) || empty($body["id_users"]) ||
		!isset($body["id_jabatan"]) || empty($body["id_jabatan"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[1] // Pegawai
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat pegawai", null);
	}

	public function update($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body) || empty($body)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[1] // Pegawai
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil mengubah pegawai", null);
	}

	public function delete($id)
	{
		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id_pegawai" => $id),
			$this->DataEnum->tableName()[6] // Seksi Pegawai
		);

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array(
				"id_solver" => $id,
				"status !=" => "antrian",
				"status !=" => "selesai"
			),
			array(
				"id_solver" => NULL,
				"is_approve" => 0,
				"status" => "antrian",
				"updated_at" => date("Y-m-d H:i:s")
			),
			$this->DataEnum->tableName()[4] // Tiket
		);

		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id" => $id),
			$this->DataEnum->tableName()[1] // Pegawai
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, null, "Berhasil menghapus pegawai", null);
	}
}
