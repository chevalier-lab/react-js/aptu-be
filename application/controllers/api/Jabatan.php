<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		// Load Models
		$this->load->model("Request");
		$this->load->model("CustomSQL");
		$this->load->model("DataEnum");
	}

	public function index() 
	{
		// Get Params
		$page = $this->input->get("page") ?: "0";
		$orderDireaction = $this->input->get("orderDireaction") ?: "DESC";
		$search = $this->input->get("search") ?: "";

		// Set Filter
		$limit = 12;
		$offset = $limit * ((int) $page);
		$where = "label LIKE '%$search%'";

		// Prepare Fetch Data
		$data = $this->CustomSQL->query("
			SELECT * FROM jabatan WHERE $where
			ORDER BY at $orderDireaction
			LIMIT $limit OFFSET $offset
		")->result_array();

		$total = $this->CustomSQL->query("
			SELECT COUNT(id) as total FROM jabatan WHERE $where
			ORDER BY at $orderDireaction
		")->row()->total;
		
		// Return If Success
		$this->Request->res(200, $data, "Berhasil memuat data jabatan", array(
			"page" => $page,
			"totalPage" => (int)($total / $limit),
			"orderDirection" => $orderDireaction,
			"search" => $search
		));
	}

	public function create()
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body["label"]) || empty($body["label"])) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->create(
			$body,
			$this->DataEnum->tableName()[0] // Jabatan
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil membuat jabatan", null);
	}

	public function update($id)
	{
		// Get Body
		$body = $this->Request->raw();

		// Check
		if (!isset($body) || empty($body)) 
			$this->Request->res(400, null, "Parameter tidak benar", null);

		// Sent to db
		$checkID = $this->CustomSQL->update(
			array("id" => $id),
			$body,
			$this->DataEnum->tableName()[0] // Jabatan
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, $body, "Berhasil mengubah jabatan", null);
	}

	public function delete($id)
	{
		// Sent to db
		$checkID = $this->CustomSQL->update(
			array(
				"id_jabatan" => $id
			),
			array(
				"id_jabatan" => NULL,
				"at" => date("Y-m-d H:i:s")
			),
			$this->DataEnum->tableName()[1] // Pegawai
		);

		// Sent to db
		$checkID = $this->CustomSQL->delete(
			array("id" => $id),
			$this->DataEnum->tableName()[0] // Jabatan
		);

		// Return If Error
		if ($checkID <= 0) $this->Request->res(500, null, "Terjadi kesalahan pada query", null);
		
		// Return If Success
		$this->Request->res(200, null, "Berhasil menghapus jabatan", null);
	}
}
