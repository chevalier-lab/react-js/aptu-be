<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Model {

    public function raw() {
        return json_decode($this->input->raw_input_stream, true);
    }

    public function res($code, $data, $message, $meta) {
        $temp = array(
            "code" => $code,
            "message" => $message
        );

        if (isset($data) || !empty($data)) $temp["data"] = $data;
        if (isset($meta) || !empty($meta)) $temp["meta"] = $meta;

        header('Content-Type: application/json');

        die(json_encode(
            $temp,
            JSON_PRETTY_PRINT
        ));
    }

    public function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}