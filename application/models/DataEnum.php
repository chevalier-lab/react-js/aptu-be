<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataEnum extends CI_Model {
    // Table Name
    public function tableName() {
        return array(
            "jabatan", // 0
            "pegawai", // 1
            "rating", // 2
            "seksi", // 3
            "tiket", // 4
            "users", // 5
            "seksi_pegawai" // 6
        );
    }

    // Platform
    public function platform() {
        return array(
            "website",
            "mobile"
        );
    }

    // Level
    public function level() {
        return array(
            'admin', // 0
            'apt', // 1
            'supervisor', //2
            'seksi', //3
            'client', //4
            'management' //5
        );
    }

    // JK
    public function jk() {
        return array(
            'm', 
            'f', 
            'p', 
            'w'
        );
    }

    // Status Tiket
    public function statusTiket() {
        return array(
            'antrian', // 0
            'diproses apt', // 1
            'diproses supervisor', // 2
            'diproses seksi', // 3
            'selesai' // 4
        );
    }
}